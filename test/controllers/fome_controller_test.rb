require 'test_helper'

class FomeControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get fome_new_url
    assert_response :success
  end

  test "should get index" do
    get fome_index_url
    assert_response :success
  end

  test "should get edit" do
    get fome_edit_url
    assert_response :success
  end

end
