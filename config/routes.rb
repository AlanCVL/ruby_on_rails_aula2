Rails.application.routes.draw do
  
  get 'form/new', to: 'form#new', as: :new_form
  get 'form/index', to:'form#index', as: :forms
  get 'form/edit', to:'form#edit', as: :edit_form
  post 'form', to: 'form#create', as: :create_form
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # EXERCICIO AULA_2
  get '/destino', to: 'form#destiny', as: :destiny
  post '/destino', to: 'form#destiny', as: :post_destiny
  get 'fim', to:'form#final', as: :final
  
end
